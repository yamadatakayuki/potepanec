class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include Spree::Core::ControllerHelpers::Store
  include Spree::Core::ControllerHelpers
  helper Spree::BaseHelper
end
