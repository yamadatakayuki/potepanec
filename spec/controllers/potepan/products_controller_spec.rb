require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "http status success " do
      expect(response.status).to eq(200)
    end

    it "render show " do
      expect(response).to render_template(:show)
    end

    it "assigns product" do
      expect(assigns(:product)).to eq product
    end
  end
end
